package projet.aspects;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import projet.exceptions.InvalidInputException;
import projet.models.User;

@Component
@Aspect
public class ProtectAgainstAdminClaimAspect {

	@Around("@annotation(projet.aspects.ProtectAgainstAdminClaim) && args(projet.models.User)")
	public void protect(ProceedingJoinPoint pjp) throws Throwable {
		User u  = (User)pjp.getArgs()[0];
		if (u.getEmail().contains("admin"))
			throw new InvalidInputException("no user email can contains \"admin\" you pirate");
		pjp.proceed();
	}
	
}
