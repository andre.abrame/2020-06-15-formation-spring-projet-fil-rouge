package projet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjetCoreSpringbootApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjetCoreSpringbootApplication.class, args).close();
	}

}
