package projet.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import projet.aspects.ProtectAgainstAdminClaim;
import projet.models.User;
import projet.repositories.UserRepository;

@Service
public class UserService {

	@Autowired
	private UserRepository md;

	public User findById(int id) {
		return md.findById(id).orElseThrow();
	}

	public List<User> findAll() {
		return md.findAll();
	}

	@ProtectAgainstAdminClaim
	public void save(User m) {
		md.save(m);
	}

	@ProtectAgainstAdminClaim
	public void update(User m) {
		md.save(m);
	}

	public void delete(User m) {
		md.delete(m);
	}

	public void deleteById(int id) {
		md.deleteById(id);
	}

	public User findByEmail(String email) {
		return md.findByEmail(email).orElseThrow();
	}

	public User findByEmailAndPassword(String email, String password) {
		return md.findByEmailAndPassword(email, password).orElseThrow();
	}

}
