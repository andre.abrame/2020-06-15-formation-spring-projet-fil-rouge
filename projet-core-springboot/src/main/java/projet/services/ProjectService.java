package projet.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import projet.models.Project;
import projet.models.User;
import projet.repositories.ProjectRepository;
import projet.repositories.UserRepository;

@Service
public class ProjectService {

	@Autowired
	private ProjectRepository pd;
	@Autowired
	private UserRepository ud;

	@Transactional
	public void save(Project p) {
		p.getOwner().getProjects().add(p);
		pd.save(p);
		ud.save(p.getOwner());
	}

	public void delete(Project e) {
		pd.delete(e);
	}

	public void deleteById(int id) {
		pd.deleteById(id);
	}

	public void update(Project e) {
		pd.save(e);
	}

	public Project findById(int id) {
		return pd.findById(id).orElseThrow();
	}

	public List<Project> findAll() {
		return pd.findAll();
	}

	public List<Project> findByNameOrDescriptionLike(String input) {
		return pd.findByNameOrDescriptionLike(input);
	}
	
	public List<Project> findByOwner(User u) {
		return pd.findByOwner(u);
	}

}
