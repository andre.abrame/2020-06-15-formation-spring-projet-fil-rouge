package projet.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import projet.validators.ForbidAdminClaim;

/**
 * Class representing a basic user. 
 * @author andre
 *
 */
@Entity
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
public class User implements Serializable {

	@Id
	@GeneratedValue
	protected int id;
	protected String password;
	
	@NotEmpty
	@ForbidAdminClaim(message = "email cannot contains the word \"admin\"")
	@Column(unique = true)
	protected String email;
	
	@OneToMany(cascade = CascadeType.ALL)
	@JsonIgnore
	protected List<Project> projects = new ArrayList<>();
	
	String role = "";

	public User() {
		super();
	}

	public User(String password, String email) {
		super();
		this.password = password;
		this.email = email;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPassword() {
		return password;
	}

	public User setPassword(String password) {
		this.password = password;
		return this;
	}

	public String getEmail() {
		return email;
	}

	public User setEmail(String email) {
		this.email = email;
		return this;
	}
	
	public List<Project> getProjects() {
		return projects;
	}

	public void setProjects(List<Project> projects) {
		this.projects = projects;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	@Override
	public String toString() {
		return "Member [password=" + password + ", email=" + email + ", id=" + id + "]";
	}

}
