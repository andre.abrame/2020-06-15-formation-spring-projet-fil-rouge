package projet.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import projet.models.Project;
import projet.models.User;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Integer> {

	@Query("from Project p where p.name like %?1% or p.description like %?1%")
	public List<Project> findByNameOrDescriptionLike(String input);

	public List<Project> findByOwner(User u);
}
