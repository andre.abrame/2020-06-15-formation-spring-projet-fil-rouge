package projet.services;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import projet.repositories.ProjectRepository;
import projet.repositories.UserRepository;

@SpringBootTest
public class ServiceTestBase {

	@MockBean
	protected UserRepository ur;

	@MockBean
	protected ProjectRepository pr;
	
}
