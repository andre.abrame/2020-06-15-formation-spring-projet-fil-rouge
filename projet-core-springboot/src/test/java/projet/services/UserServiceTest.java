package projet.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.NoSuchElementException;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import projet.models.User;

@SpringBootTest
class UserServiceTest extends ServiceTestBase {

	@Autowired
	UserService us;
	
	
	@Test
	void findById_success() {
		// data fixtures
		User u = new User();
		// configure mock
		when(ur.findById(Mockito.anyInt())).thenReturn(Optional.of(u));
		// call tested method
		User r = us.findById(123);
		// state tests
		assertEquals(u, r, "findById should return the expected user");
		// behavior tests
		verify(ur, times(1)).findById(123);
		verifyNoMoreInteractions(ur);
	}

	@Test
	void findById_error() {
		// data fixtures
		// configure mock
		doThrow(NoSuchElementException.class).when(ur).findById(Mockito.anyInt());
		// call tested method & state tests
		assertThrows(NoSuchElementException.class, () -> us.findById(123), "findById should throw a NoSuchElementException when the id does not exist");
		// behavior tests
		verify(ur, times(1)).findById(123);
		verifyNoMoreInteractions(ur);
	}

}
