package projet.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ForbidAdminClaimValidator implements ConstraintValidator<ForbidAdminClaim, String> {

	
	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		return value == null || !value.toLowerCase().contains("admin");
	}
	

}
