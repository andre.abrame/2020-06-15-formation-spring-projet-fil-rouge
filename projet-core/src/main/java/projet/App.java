package projet;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import projet.configuration.ApplicationConfig;
import projet.models.User;
import projet.services.UserService;

public class App {

	public static void main(String[] args) {
		
		ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
		
		UserService us = context.getBean(UserService.class);
		
		us.save(new User("a", "a"));
		System.out.println(us.findById(1));
	}

}
