package projet.exceptions;

public class BugException extends RuntimeException {

	public BugException() {
	}

	public BugException(String message) {
		super(message);
	}

	public BugException(Throwable cause) {
		super(cause);
	}

	public BugException(String message, Throwable cause) {
		super(message, cause);
	}

	public BugException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
