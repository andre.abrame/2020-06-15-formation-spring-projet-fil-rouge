package projet.aspects;

import javax.persistence.NoResultException;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.hibernate.exception.JDBCConnectionException;
import org.springframework.stereotype.Component;

import projet.exceptions.BugException;
import projet.exceptions.DatabaseAccessException;
import projet.exceptions.InvalidInputException;

@Component
@Aspect
public class ExceptionEncapsulatorAspect {

	@Around("execution(* projet.services..*(..))")
	public Object encapsulate(ProceedingJoinPoint pjp) {
		try {
			return pjp.proceed();
		} catch (NoResultException ex) {
			throw new InvalidInputException(ex);
		} catch(JDBCConnectionException ex) {
			throw new DatabaseAccessException(ex);
		} catch(Throwable ex) {
			throw new BugException(ex);
		}
	}
	
}
