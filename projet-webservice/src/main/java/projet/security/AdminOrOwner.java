package projet.security;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import org.springframework.security.access.prepost.PreAuthorize;

@Retention(RUNTIME)
@Target(METHOD)
@PreAuthorize("hasRole('ADMIN') or (@projectService.findById(new Integer(#id)).getOwner() == @userService.findByEmail(principal.getUsername()))")
public @interface AdminOrOwner {

}
