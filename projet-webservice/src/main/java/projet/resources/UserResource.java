package projet.resources;

import java.security.Principal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import projet.models.Project;
import projet.models.User;
import projet.services.ProjectService;
import projet.services.UserService;

@RestController
@RequestMapping("user")
public class UserResource {
	
	@Autowired
	private UserService us;
	
	@Autowired
	private ProjectService ps;

	@GetMapping
	public List<User> findAll() {
		return us.findAll();
	}

	@GetMapping("{id}")
	public User findById(@PathVariable int id) {
		return us.findById(id);
	}

	@GetMapping("{id}/projects")
	public List<Project> findByOwner(@PathVariable int id) {
		return ps.findByOwner(us.findById(id));
	}

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public void save(@Validated @RequestBody User u) {
		us.save(u);
	}

	@PutMapping("{id}")
	public void update(@Validated @RequestBody User u, @PathVariable int id) {
		u.setId(id);
		us.update(u);
	}
	
	@DeleteMapping("{id}")
	public void delete(@PathVariable int id) {
		us.deleteById(id);
	}
	
}
