package projet.resources;

import java.security.Principal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import projet.models.Project;
import projet.security.AdminOrOwner;
import projet.services.ProjectService;
import projet.services.UserService;

@RestController
@RequestMapping("project")
public class ProjectResource {
	
	@Autowired
	private UserService us;
	
	@Autowired
	private ProjectService ps;

	@GetMapping
	public List<Project> findAll(HttpServletRequest request) {
		if (request.isUserInRole("ROLE_ADMIN"))
			return ps.findAll();
		return ps.findByOwner(us.findByEmail(request.getUserPrincipal().getName()));
	}

	@GetMapping("{id}")
	@AdminOrOwner
	public Project findById(@PathVariable int id) {
		return ps.findById(id);
	}

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public void save(@Validated @RequestBody Project u, Principal principal) {
		u.setOwner(us.findByEmail(principal.getName()));
		ps.save(u);
	}

	@PutMapping("{id}")
	@AdminOrOwner
	public void update(@Validated @RequestBody Project u, @PathVariable int id) {
		u.setId(id);
		ps.update(u);
	}
	
	@DeleteMapping("{id}")
	@AdminOrOwner
	public void delete(@PathVariable int id) {
		ps.deleteById(id);
	}
	
}
