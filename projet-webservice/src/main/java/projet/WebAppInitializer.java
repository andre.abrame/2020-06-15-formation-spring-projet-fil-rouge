package projet;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import projet.configuration.WebappConfig;

public class WebAppInitializer implements WebApplicationInitializer {

	@Override
	public void onStartup(final ServletContext sc) throws ServletException {
		// Create the dispatcher servlet's Spring application context
		AnnotationConfigWebApplicationContext dispatcherContext = new AnnotationConfigWebApplicationContext();
		dispatcherContext.register(WebappConfig.class); // change to point to your configuration class(es)
		sc.addListener(new ContextLoaderListener(dispatcherContext));
		// Register the dispatcher servlet
		ServletRegistration.Dynamic appServlet = sc.addServlet("webapp", new DispatcherServlet(dispatcherContext));
		appServlet.setLoadOnStartup(1);
		appServlet.addMapping("/");
	}

}