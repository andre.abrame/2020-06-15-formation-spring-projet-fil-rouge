# 2020-06-15 - Formation Spring - Projet fil rouge

Un projet maven multi-module qui sera "springifié" au cours de la formation. Il se compose des modules suivants :
- projet-core : Le noyau de l'application, inculant les entités métiers, les repositories (DAO) et les services
- projet-webapp : une application web simple, basé sur servlet/JSP (à venir)
- projet-webservice : un webservice classique utilisant JAX-RS/jersey et jackson pour la serialisation/deserialisation JSON (à venir)

Des modules suplémentaires seront ajoutés au fur et à mesure de la formation.