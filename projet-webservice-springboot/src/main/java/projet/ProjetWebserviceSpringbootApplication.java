package projet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
public class ProjetWebserviceSpringbootApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjetWebserviceSpringbootApplication.class, args);
	}

}
