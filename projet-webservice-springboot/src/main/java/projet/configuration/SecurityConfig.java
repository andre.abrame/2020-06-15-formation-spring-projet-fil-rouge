package projet.configuration;

import java.util.Arrays;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.OrRequestMatcher;

import projet.models.User;
import projet.services.UserService;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Bean
	public UserDetailsService userDetailService() {
		return new UserDetailsService() {
			@Autowired
			UserService us;

			@Override
			public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
				try {
					User u = us.findByEmail(username);
					return new org.springframework.security.core.userdetails.User(u.getEmail(), u.getPassword(),
							Arrays.asList(new SimpleGrantedAuthority(u.getRole())));
				} catch (NoSuchElementException e) {
					throw new UsernameNotFoundException("no user with this email was found", e);
				}
			}
		};
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.sessionManagement()
			.sessionCreationPolicy(SessionCreationPolicy.STATELESS)
			.and().authorizeRequests()
				.mvcMatchers(HttpMethod.GET, "/user/**").authenticated()
				.mvcMatchers(HttpMethod.POST, "/user").permitAll()
				.mvcMatchers(HttpMethod.PUT, "/user/{id}")
					.access("hasRole('ADMIN') or (new Integer(#id) == @userService.findByEmail(principal.getUsername()).getId())")
				.mvcMatchers(HttpMethod.DELETE, "/user/{id}")
					.access("hasRole('ADMIN') or (new Integer(#id) == @userService.findByEmail(principal.getUsername()).getId())")
				.requestMatchers(new OrRequestMatcher(new AntPathRequestMatcher("/project", "GET"),
													  new AntPathRequestMatcher("/project", "POST")))
					.authenticated()
				.mvcMatchers(HttpMethod.GET, "/project/{id}")
					.access("hasRole('ADMIN') or (@projectService.findById(new Integer(#id)).getOwner() == @userService.findByEmail(principal.getUsername()))")
				.mvcMatchers(HttpMethod.PUT, "/project/{id}")
					.access("hasRole('ADMIN') or (@projectService.findById(new Integer(#id)).getOwner() == @userService.findByEmail(principal.getUsername()))")
				.mvcMatchers(HttpMethod.DELETE, "/project/{id}")
					.access("hasRole('ADMIN') or (@projectService.findById(new Integer(#id)).getOwner() == @userService.findByEmail(principal.getUsername()))")
				.anyRequest().denyAll()
			// enabling CORS (exempt from authentication OPTIONS requests)
			.and().cors()
			// disabling CSRF necessary ?
			.and().csrf().disable()
			// disabling default login
			.formLogin().disable()
			.httpBasic().and()
			// disabling default logout
			.logout().disable();
	}

}
